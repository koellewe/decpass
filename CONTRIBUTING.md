# Contributing to decpass

Here's the general procedure:
- Pick an issue, or create one if addressing a new issue or adding a new feature
- Clone repo at `master` and create a new branch called `issue/n` where `n` is the issue ID.
- Work on and Push `issue/n`
- Create a merge request for `issue/n` -> `staging`.
- If accepted, your contribution will be part of a future release.
    - A release will be marked by a successful merge request from `staging` -> `master`.
    - It will also include a new release tag and binary distribution.
