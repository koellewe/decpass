# DecPass

An open-source terminal-based decentralised password manager for Linux.

<a href="https://siasky.net/AABRvepCZxosoPTKNIIBZUsdjmn_D_PghVp8fBAfN7IbWA">
  <img src="https://siasky.net/DADGezv68oQ6CV2CeKplxHcpBSZGodWMrtIPtHFZvQiLow" width="542" alt="DecPass demonstration video"/>
</a>

## Getting started

If you're on a Debian-based distro, you can grab the packaged deb from [the repo's releases section](https://gitlab.com/koellewe/decpass/-/releases).
It will install all the dependencies and make the program runnable from anywhere. 

```shell
sudo apt install ./decpass_*.deb  # whatever the version

# execute from anywhere!
decpass
```

If you're not on Debian, you'll have to build the program yourself. See [the wiki entry](https://gitlab.com/koellewe/decpass/-/wikis/Building).

## Everything else

Check the [wiki](https://gitlab.com/koellewe/decpass/-/wikis/home).
