// todo else if time out of sync
#ifndef DECPASS_TARDIGRADE_H
#define DECPASS_TARDIGRADE_H

#include <string>
#include <uplink/uplink.h>

using namespace std;

// I know Tardigrade is being rebranded as "Storj DCS", but I'm keeping the name because it's relevant enough, but also
// different enough to not clash with the uplink lib.
class Tardigrade {
public:
    Tardigrade(const char* access_grant, const char* bucket);
    ~Tardigrade();

    long dbLastUpdated();

    void upload(const string &localfile, const string &remotefile);
    void download(const string &remotefile, const string &localfile, bool safePerms = true);

    string error;

private:
    UplinkProject *proj;
    string bucket;

};


#endif //DECPASS_TARDIGRADE_H
