
#ifndef DECPASS_SYNCHRONISER_H
#define DECPASS_SYNCHRONISER_H
#include "../local/DatabaseManager.h"
#include "../tard/Tardigrade.h"
#include "../local/ConfigManager.h"


class Synchroniser {
public:
    Synchroniser(DatabaseManager *db, Tardigrade *tard, ConfigManager *cfg);
    int sync();

private:
    DatabaseManager *db;
    Tardigrade *tard;
    ConfigManager *cfg;
};


#endif //DECPASS_SYNCHRONISER_H
