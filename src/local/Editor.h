
#ifndef DECPASS_EDITOR_H
#define DECPASS_EDITOR_H


#include "../data/Login.h"
#include "../data/Note.h"
#include <string>

class Editor {
public:
    static SecureItem* editItem(SecureItem* item);
    static SecureItem* newItem(bool login, const std::string &name);

private:
    static SecureItem* itemFromFile(bool is_login, const std::string &path);

};


#endif //DECPASS_EDITOR_H
