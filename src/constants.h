
#ifndef DECPASS_CONSTANTS_H
#define DECPASS_CONSTANTS_H

namespace constants
{
    const char* const DB_NAME = "decpass.db";
    const int BIN_BUFF = 1024;
}


#endif //DECPASS_CONSTANTS_H
