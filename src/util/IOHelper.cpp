
#include "IOHelper.h"
#include <sys/stat.h>
#include <iostream>
#include <ctime>

bool IOHelper::PathExists(const std::string *path) {
    struct stat buffer{};
    int res = stat(path->c_str(), &buffer);
    return res == 0;
}

long IOHelper::LastUpdated(const std::string *path) {
    struct stat res{};
    if (stat(path->c_str(), &res) == 0){
        return res.st_mtime; // macro that gets the unix time

    }else{
        std::cerr << "Error reading metadata from " << *path << std::endl;
        return -1;
    }
}
