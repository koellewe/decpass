
#ifndef DECPASS_REPL_H
#define DECPASS_REPL_H
#include "../local/DatabaseManager.h"
#include "../tard/Tardigrade.h"
#include "../local/ConfigManager.h"
#include "../tard/Synchroniser.h"
#include <string>


class Repl {
public:
    Repl(DatabaseManager *db, Synchroniser *sync);
    void run();

private:
    DatabaseManager *db;
    Synchroniser *sync;

    // internal procedures
    void search(std::string &query);
    void modify(bool login, std::string &name);
    void deleteItem(const std::string &name);

};


#endif //DECPASS_REPL_H
