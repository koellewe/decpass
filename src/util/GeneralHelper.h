
#ifndef DECPASS_GENERALHELPER_H
#define DECPASS_GENERALHELPER_H

#include <string>
#include <iostream>
#include <istream>

class GeneralHelper {
public:

    static void printLines(const std::string &content, short indentation = 0, std::ofstream *filestream = nullptr);

};


#endif //DECPASS_GENERALHELPER_H
