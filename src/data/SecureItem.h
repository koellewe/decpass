
#ifndef DECPASS_SECUREITEM_H
#define DECPASS_SECUREITEM_H

class SecureItem {
public:
    enum ITEM_TYPE{LOGIN, NOTE};

    SecureItem(const std::string &name) : name(name) {}

private:
    std::string name;

protected:
    ITEM_TYPE itemType;

public:
    const std::string &getName() const {
        return name;
    }

    const ITEM_TYPE getType() const {
        return itemType;
    }
};

#endif //DECPASS_SECUREITEM_H
