#include <iostream>
#include <string>
#include "src/local/ConfigManager.h"
#include "src/tard/Tardigrade.h"
#include "src/local/DatabaseManager.h"
#include "src/tard/Synchroniser.h"
#include "src/util/Repl.h"

using namespace std;

int main() {

    cout << "Welcome to DecPass" << endl;

    // === CONFIG ===

    auto *cfg = new ConfigManager();
    Tardigrade *tard = nullptr;

    if (!cfg->isConfigured()) {

        cout << "Running first time config..." << endl;

        cout << "Guide here: https://gitlab.com/koellewe/decpass/-/wikis/Storj-setup" << endl;

        string access_grant;
        cout << "Access grant: ";
        cin >> access_grant;

        string bucket;
        cout << "Bucket name: ";
        cin >> bucket;

        cout << "Checking details..." << endl;

        tard = new Tardigrade(access_grant.c_str(), bucket.c_str());
        if (!tard->error.empty()){
            cerr << tard->error << endl;
            return 1;
        }

        cout << "Config looks good. Saving to " << cfg->getConfigFile() << endl;
        cfg->setAccessGrant(access_grant);
        cfg->setBucket(bucket);
        cfg->setConfigured(true);
        cfg->writeOut();

        // Config sorted.

    }else{
        tard = new Tardigrade(cfg->getAccessGrant().c_str(), cfg->getBucket().c_str());
        cout << "Config loaded." << endl; // assuming good (from last storage)
    }

    // === SYNC ===

    auto *db = new DatabaseManager(cfg->getAppDir());
    auto *sync = new Synchroniser(db, tard, cfg);
    // internally creates db if nec
    sync->sync();

    // === REPL ===

    db->connectLocalDB();
    if (db->isError()){
        cerr << db->getError() << endl; return 1;
    }

    auto *repl = new Repl(db, sync);
    repl->run();


    // fin
    delete repl;
    delete sync;
    delete db;
    delete tard;
    delete cfg;
    return 0;
}
